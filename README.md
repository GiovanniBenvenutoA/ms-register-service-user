# ms-register-service-user

[![Build Status](https://travis-ci.org/codecentric/springboot-sample-app.svg?branch=master)](https://travis-ci.org/codecentric/springboot-sample-app)
[![License](http://img.shields.io/:license-apache-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0.html)


## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven](https://maven.apache.org)
- [SpringBooot](https://spring.io/projects/spring-boot)
- [JWT](https://jwt.io/)
- [SWAGGER](https://swagger.io/specification/)


## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.ms.registration.user.MsUserRegistrationBciproofApplication` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn clean package
```
```shell
mvn install
```
this will create a jar of the project in which it will remain in the path this project ms-user-registration-bciproof/target/ms-user-registration-bciproof-0.0.1-SNAPSHOT.jar
```shell
mvn spring-boot:run
```
you can execute the execution from your favorite ide and if it is not possible you can execute the command by cmd or terminal java- jar in the root folder where you find the project jar
java -jar ms-user-registration-bciproof-0.0.1-SNAPSHOT.jar

after running the application to access database h2 is http://localhost:8080/h2-console/login.jsp?jsessionid=b5dbebf3d8e5e3e56ef23739a2f4be7f

and the api-doc with swagger is http://localhost:8080/swagger-ui/index.html#/ in swagger you will find the methods used for this development

package com.ms.registration.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsUserRegistrationBciproofApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsUserRegistrationBciproofApplication.class, args);
	}
}

package com.ms.registration.user.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ms.registration.user.entities.Login;
import com.ms.registration.user.entities.MensajeRespuesta;
import com.ms.registration.user.entities.RegisterOk;
import com.ms.registration.user.entities.ResponseMessageStatus;
import com.ms.registration.user.entities.UpdateUser;
import com.ms.registration.user.service.UserService;

import io.swagger.v3.oas.annotations.Operation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping({ "/api" })
public class UserRegisterController {
	

	
	@Autowired
	UserService userService;
	
	@Operation(summary = "add new User")
	
	@PostMapping("/register")
	public ResponseEntity<?> login(@RequestBody Login request) {
		
		ResponseEntity<?> response= null;
		response=userService.InsertData(request);
		return response;
	}
	
	@Operation(summary = "Status Validate User")
	@GetMapping("/statusUser")
	public ResponseEntity<?> getStatusUser(String id){
		
		ResponseMessageStatus message = new ResponseMessageStatus();
		message=userService.getStatusUser(id);
		return new ResponseEntity<>(message,HttpStatus.OK);
	
	}
	
	@Operation(summary = "Update Validate User")
	@PutMapping("/updateUser")
	public ResponseEntity<?> stateUpdateUser(@RequestBody UpdateUser request){
		ResponseMessageStatus message = new ResponseMessageStatus();
		message=userService.updateUser(request.getId(),request.isActive());
		return new ResponseEntity<>(message,HttpStatus.OK);
	}
	

}

package com.ms.registration.user.entities;
import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Login implements Serializable{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4898277275234621683L;
	private String name;
	private String email;
	private String password;
	private List<Phones> phones;
	
	
	
	
	

}

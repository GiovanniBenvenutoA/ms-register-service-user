package com.ms.registration.user.entities;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MensajeExitoso implements Serializable {
	
	
	

	private static final long serialVersionUID = 1384717503270461821L;
	
	private String id;
	private String created;
	private String modified;
	private String last_login;
	private String token;
	private String isactive;
	
	
	
	
	
	

}

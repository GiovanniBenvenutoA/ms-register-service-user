package com.ms.registration.user.entities;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MensajeRespuesta implements Serializable{
	
	
	private static final long serialVersionUID = 6623452273584907905L;
	private int codigo;
	private String mensaje;
	
	
	
	
	

}

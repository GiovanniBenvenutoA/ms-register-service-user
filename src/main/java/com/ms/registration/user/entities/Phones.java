package com.ms.registration.user.entities;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Phones implements Serializable{
	

	private static final long serialVersionUID = 8031798544136721755L;
	private String number;
	private String citycode;
	private String contrycode;
	
	
	
	

}

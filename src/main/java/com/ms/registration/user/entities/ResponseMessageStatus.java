package com.ms.registration.user.entities;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ResponseMessageStatus implements Serializable {
	
	
	private static final long serialVersionUID = -6433205026343923118L;
	
	private int codigo;
	private String mensaje;
	private boolean result;

}

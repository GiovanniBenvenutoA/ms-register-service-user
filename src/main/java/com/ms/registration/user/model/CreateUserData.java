package com.ms.registration.user.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="users")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateUserData implements Serializable{
	
	private static final long serialVersionUID = 7237399352283759781L;
	@Id
	@Column(name="id")
	private String id;
	@Column(name="name")
	private String name;
	@Column(name="email")
	private String email;
	@Column(name="password")
	private String password;
	@Column(name="created")
	private LocalDateTime created;
	@Column(name="modified")
	private LocalDateTime modified;
	@Column(name="last_login")
	private LocalDateTime last_login;
	@Column(name="isactive")
	private boolean isactive;
	@Column(name="token")
	private String token;
	

}

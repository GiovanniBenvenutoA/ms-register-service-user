package com.ms.registration.user.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="phones")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PhonesUser implements Serializable {
	
	
	private static final long serialVersionUID = -3632323547382816275L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name="user_reference")
	private String user_reference;
	@Column(name="phones_number")
	private String phones_number;
	@Column(name="phones_citycode")
	private String phones_citycode;
	@Column(name="phones_contrycode")
	private String phones_contrycode;

}

package com.ms.registration.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ms.registration.user.model.PhonesUser;

public interface PhonesRepository extends JpaRepository<PhonesUser, String>{
	
	

}

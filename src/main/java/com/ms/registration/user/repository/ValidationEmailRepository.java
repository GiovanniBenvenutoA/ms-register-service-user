package com.ms.registration.user.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ms.registration.user.model.CreateUserData;

@Repository
public interface ValidationEmailRepository extends JpaRepository<CreateUserData, String>{
	
	@Query(value="select * from users  where email =  ?1", nativeQuery = true)
	String findByEmail(String mail);
	
	@Query(value="select isactive from users  where id =  ?1", nativeQuery = true)
	String findByStatus(String id);
	
	@Modifying
	@Transactional
	@Query(value="update users set isactive = ?2  where id =  ?1", nativeQuery = true)
	void updateUser(String id,boolean active);
	
	

}

package com.ms.registration.user.service;

import com.ms.registration.user.entities.Login;
import com.ms.registration.user.entities.RegisterOk;
import com.ms.registration.user.entities.ResponseMessageStatus;
import org.springframework.http.ResponseEntity;

public interface UserService {

	boolean validateEmail(String email);

	ResponseEntity<?> InsertData(Login request);

	
	ResponseMessageStatus getStatusUser(String id);

	ResponseMessageStatus updateUser(String id,boolean active);

}

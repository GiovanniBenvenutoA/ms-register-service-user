package com.ms.registration.user.service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.ms.registration.user.entities.MensajeRespuesta;
import com.ms.registration.user.util.Validation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.ms.registration.user.entities.Login;
import com.ms.registration.user.entities.RegisterOk;
import com.ms.registration.user.entities.ResponseMessageStatus;
import com.ms.registration.user.model.CreateUserData;
import com.ms.registration.user.model.PhonesUser;
import com.ms.registration.user.repository.PhonesRepository;
import com.ms.registration.user.repository.ValidationEmailRepository;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class UserServiceImpl implements UserService{
	
private static Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
	ValidationEmailRepository userDataJpa;
	
	@Autowired
	PhonesRepository phonesRepository;


	
	@Override
	public boolean validateEmail(String email) {
			boolean value =false;
			String respuesta = userDataJpa.findByEmail(email);
			if(respuesta!=null) {
				value=true;
			}else {
				value=false;
			}
		 return value;
		
	}

	
	
	@Override
	public ResponseEntity<?> InsertData(Login request) {
		Validation validation=new Validation();
		MensajeRespuesta message = new MensajeRespuesta();
		ResponseEntity<?> reponse;
		UUID uuid = UUID.randomUUID();
		CreateUserData user = new CreateUserData();
		RegisterOk registerfinal=null;
		LocalDateTime createUser = LocalDateTime.now();
		LocalDateTime modifyUser = LocalDateTime.now();
		LocalDateTime lastLogin = LocalDateTime.now();
		boolean validatePassword=validation.validaPassword(request.getPassword());
		boolean validaemail=validateEmail(request.getEmail());
		boolean formatvalidate=validation.validaEmail(request.getEmail());
		if(validaemail==false && formatvalidate==true) {
			if(validatePassword==true) {
				try {
					user.setId(uuid.toString());
					user.setName(request.getName());
					user.setEmail(request.getEmail());
					user.setPassword(request.getPassword());
					for(int i=0;i<request.getPhones().size();i++) {
						PhonesUser phone = new PhonesUser();
						phone.setUser_reference(uuid.toString());
						phone.setPhones_number(request.getPhones().get(i).getNumber());
						phone.setPhones_citycode(request.getPhones().get(i).getCitycode());
						phone.setPhones_contrycode(request.getPhones().get(i).getContrycode());
						phonesRepository.save(phone);
					}
					user.setCreated(createUser);
					user.setModified(modifyUser);
					user.setLast_login(lastLogin);
					user.setIsactive(true);
					user.setToken(uuid.toString());
					userDataJpa.save(user);
					registerfinal=asigna_registro(user);
					return new ResponseEntity<>(registerfinal,HttpStatus.CREATED);
				} catch (Exception e) {
					log.error("error"+e);
				}
			}
		}else {
			message.setCodigo(-1);
			message.setMensaje("Correo a registrado ya existe");
			return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
		}
		message.setCodigo(-2);
		message.setMensaje("algun parametro o valor no esta incluido,favor revisar");
		return new ResponseEntity<>(registerfinal,HttpStatus.BAD_REQUEST);
	}
	
	@Override
	public ResponseMessageStatus getStatusUser(String id) {
		
		
		ResponseMessageStatus message = new ResponseMessageStatus();
		String resultSearch=userDataJpa.findByStatus(id);
		if(resultSearch==null) {
			message.setCodigo(-1);
			message.setMensaje("Usuario Inexistente");
			message.setResult(true);
		}else {
			switch(resultSearch) {
			case "true":
				message.setCodigo(0);
				message.setMensaje("Usuario Activo");
				message.setResult(true);
				break;
			case "false":
				message.setCodigo(1);
				message.setMensaje("Usuario Inactivo");
				message.setResult(true);
				break;
		}
		}
		return message;
	}
	
	
	@Override
	public ResponseMessageStatus updateUser(String id,boolean active) {
		ResponseMessageStatus message = new ResponseMessageStatus();
		String resultSearch=userDataJpa.findByStatus(id);
		if(resultSearch==null) {
			message.setCodigo(-1);
			message.setMensaje("Usuario Inexistente");
			message.setResult(true);
		}else {
			switch(resultSearch) {
				case "true":
					message.setCodigo(0);
					message.setMensaje("Usuario Actualizado");
					message.setResult(true);
					userDataJpa.updateUser(id,active);
					break;
				case "false":
					message.setCodigo(1);
					message.setMensaje("Usuario No actualizado");
					message.setResult(true);
					break;
			}
		}
		return message;

	}
	
	private RegisterOk asigna_registro(CreateUserData user) {
		RegisterOk register = new RegisterOk();
		register.setId(user.getId());
		register.setCreated(user.getCreated());
		register.setModified(user.getModified());
		register.setLastLogin(user.getLast_login());
		register.setToken(user.getToken());
		register.setIsactive(true);
		return register;
		
	}

}
